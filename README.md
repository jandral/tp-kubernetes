# tp-kubernetes

## Démarrer

### Pré-requis

- 3 VM démarrée autorisant la connexion ssh(connaître son adresse IP)
- Avoir Kubespray d'installé sur ses 3 VMs ( vérifier à l'aide des 3 commandes suivantes : ```ssh user@master-ip``` , ```sudo -i``` , ```kubectl get nodes```)
- Avoir un loadBalancer IP sur votre hébergeur Cloud qui redirige vers toutes vos machines

### Installation

Cloner le répertoire git à l'aide de git clone dans l'une de vos VMs master.

Se rendre dans le répertoire cloné (```cd tp-kubernetes```)

Lancer les commandes suivantes dans l'ordre :

### Commandes à lancer

``` bash
kubectl apply -f add-namespace.yml
```

``` bash
kubectl apply -f configure-quotas.yml --namespace=my-namespace
```

``` bash
kubectl apply -f create-deployment.yml --namespace=my-namespace
```

``` bash
kubectl apply -f create-service.yml --namespace=my-namespace
```

``` bash
helm install --name=ingress -f config/helm-ingress-nginx.yml stable/nginx-ingress
```

``` bash
kubectl apply -f configure-ingree.yml --namespace=my-namespace
```

## Auteurs

- **Julien ANDRAL** - _Développeur_
